/*
 * Read db schema json file and create table.
 */

var jsonfile = require('jsonfile'),
    fs = require('fs'),
    mysql = require('mysql'),
    async = require('async'),
    config = require('config');

var argv_dbname = process.argv.slice(2),    //執行時的參數，要get dbname
    connection,
    filepath = '',
    dbname = argv_dbname[0],
    asyncTasks = [],
    asyncTasks_sql = [],
    table_obj = {
        dbname: '',
        tablename: '',
        fields: []
    };

if(!dbname){
    console.log("Pls input the database name, ex: node readjson_create.js -{db_name}");
    return false;
}else{
    dbname = dbname.substring(1);
    readJson();   //process db schema json file
}


function readJson()
{   
    connectDB(dbname);
    
    var dirname = './dbschema/' + dbname + '/';
    fs.readdir(dirname, function(err, filenames) {
        if (err) {
            console.log("readdir " + dirname +" error : ", err);
            return;
        }

        //用async依序讀取且create table
        async.forEach(filenames, function (table, callback){
            //read file
            fs.readFile(dirname + table, 'utf-8', function(err, table_fields) {
                if (err) {
                    console.log("readfile "+ dirname + table + " error : ", err);
                    return;
                }
                
                createTable(table_fields);
                callback();
            });
        }, function() { //這邊是讀取完每一個excel後的function
            
            //用async依序執行asyncTasks_sql裡面的function
            async.series(asyncTasks_sql, function(err, results) {
                closeDB();
                console.log("read and created");
            });
        });
    });
}

/*
 * 湊出create table的sql command
 */
function createTable(table_fields)
{
    //console.log(table_fields);
    var table_fields_obj = JSON.parse(table_fields);
    
    var tmp_pk_arr = [];
    for(var f_key in table_fields_obj.fields){
        if(table_fields_obj.fields[f_key]['pk'] == "Y"){
            tmp_pk_arr.push(table_fields_obj.fields[f_key]['fname']);
        }
    }
    
    var pk_str = tmp_pk_arr.join();
    var index_str = '';
    var query = '';
    
    query = "CREATE TABLE `"+ table_fields_obj.tablename +"` "; 
    query += "( ";
    
    for(var f_key in table_fields_obj.fields){
        var type = table_fields_obj.fields[f_key]['type'];
        var type_and = '';
        
        //檢查type，有些特定type不用length
        if(type == 'datetime' || type == 'date' || type == 'time' || type == 'timestamp' || type == 'text' || type == 'float'){
            type_and = type;
        }else{
            type_and = type + '(' + table_fields_obj.fields[f_key]['length'] + ')';
        }
        
        if(f_key == 0){
            //檢查pk
            var tmp_q = (table_fields_obj.fields[f_key]['pk'] == "Y") ? " NOT NULL AUTO_INCREMENT" : " NULL";
            
            //檢查有無註解
            var tmp_comment = (table_fields_obj.fields[f_key]['comment'] != 'undefined' && !!table_fields_obj.fields[f_key]['comment']) ? " COMMENT '" + table_fields_obj.fields[f_key]['comment'] + "'" : '';
            
            //檢查有無default
            if(table_fields_obj.fields[f_key]['default'] != 'no default'){
                if(table_fields_obj.fields[f_key]['pk'] == "Y"){
                    var tmp_default = '';
                }else{
                    var tmp_default = " DEFAULT '" + table_fields_obj.fields[f_key]['default'] + "'";
                }
            }else{ 
                var tmp_default = '';
            };
            
            //檢查index
            index_str += (table_fields_obj.fields[f_key]['index'] == "Y" && table_fields_obj.fields[f_key]['pk'] != "Y") ? ", KEY `" + table_fields_obj.fields[f_key]['fname'] + "` (`" + table_fields_obj.fields[f_key]['fname'] + "`)" : "";
            
            query += "`" + table_fields_obj.fields[f_key]['fname'] + "` " + type_and + tmp_q + tmp_default + tmp_comment;
        }else{
            //檢查pk
            var tmp_q = (table_fields_obj.fields[f_key]['pk'] == "Y") ? " NOT NULL AUTO_INCREMENT" : " NULL";
            
            //檢查有無註解
            var tmp_comment = (table_fields_obj.fields[f_key]['comment'] != 'undefined' && !!table_fields_obj.fields[f_key]['comment']) ? " COMMENT '" + table_fields_obj.fields[f_key]['comment']  + "'" : '';
            
            var func_arr = new Array('CURRENT_TIMESTAMP');
            
            //檢查有無default
            if(table_fields_obj.fields[f_key]['default'] != 'no default'){
                if(table_fields_obj.fields[f_key]['pk'] == "Y"){
                    var tmp_default = '';
                }else{
                    var tmp_default;
                    if(func_arr.indexOf(table_fields_obj.fields[f_key]['default']) != -1){   //default value 為函式
                        tmp_default = " DEFAULT " + table_fields_obj.fields[f_key]['default'];
                    }else{  //default value 非函式
                        tmp_default = " DEFAULT '" + table_fields_obj.fields[f_key]['default'] + "'";
                    }
                }
            }else{ 
                var tmp_default = '';
            };
            
            //檢查index
            index_str += (table_fields_obj.fields[f_key]['index'] == "Y" && table_fields_obj.fields[f_key]['pk'] != "Y") ? ", KEY `" + table_fields_obj.fields[f_key]['fname'] + "` (`" + table_fields_obj.fields[f_key]['fname'] + "`)" : "";
            
            query += ", `" + table_fields_obj.fields[f_key]['fname'] + "` " + type_and + tmp_q + tmp_default + tmp_comment;
        }
    }
    query += ", PRIMARY KEY (" + pk_str + ") ";
    query += index_str;
    query += ") ";
    query += "ENGINE = MyISAM DEFAULT CHARSET = utf8 COMMENT='" + table_fields_obj.comment + "'";

    //將執行sql的function push到asyncTasks_sql
    asyncTasks_sql.push(function(sql_callback){
        console.log("======================================================================");
        console.log("Creating table: ", table_fields_obj.tablename);
        console.log("======================================================================");
        createTable_sql(query, table_fields_obj.tablename, sql_callback);
    });
}

function createTable_sql(query, tablename, sql_callback)
{
    connection.query('SELECT 1 FROM ' + tablename, function(error, rows, fields){
        //檢查是否有錯誤
        if(error){
            connection.query(query, function(error, rows, fields){
                console.log(query);
                //檢查是否有錯誤
                if(error){
                    console.log('create err : ', error);
                    throw error;
                }
                
                sql_callback();
            });
        }else{
            console.log('table ' + tablename + ' was exist!');
            sql_callback();
        }
    });    
}

function connectDB(dbname)
{
    console.log("DB connected");
    
    var db_config = config.database;
    connection = mysql.createConnection({
        host: db_config.host,
        user: db_config.username,
        password: db_config.password,
        database: dbname
    });

    //開始連接
    connection.connect();
    
    //console.log("END connectDB");
}

function closeDB()
{
    console.log("DB closed");
    connection.end();
}