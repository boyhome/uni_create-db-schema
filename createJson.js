/*
 * Read db schema excel and create db schema json file.
 */

var mysql = require('mysql'),
    async = require("async"),
    xlsx = require('node-xlsx'),
    util = require('util'),
    fs = require('fs');

var argv_dbname = process.argv.slice(2),    //執行時的參數，要get dbname
    argv_filename = process.argv.slice(3),  //執行時的參數，要get Excel 檔名
    dbname = argv_dbname[0],
    filename = argv_filename[0],
    obj,
    arr_push = false,
    sql_asyncTasks = [],
    table_obj = {
        dbname: '',
        tablename: '',
        fields: []
    };

if(!dbname){
    console.log("Pls input the database name, ex: node createJson.js -{db_name} -{schema_filename}");
    return false;
}else{
    if(!filename){
        console.log("Pls input the database schema file name, ex: node createJson.js -{db_name} -{schema_filename}");
        return false;
    }else{
        filename = filename.substring(1);
        obj = xlsx.parse('./' + filename); // parses schema file 
    }
    dbname = dbname.substring(1);
}
//process excel obj

var data_length = obj[0].data.length;
for(var data_key in obj[0].data){
    if(obj[0].data[data_key].length == 0){
        data_length--;
        continue;
    }
    
    //略過每個table的第一列
    if(!arr_push && obj[0].data[data_key][0] == 'Table name'){
        arr_push = true;
        
        if(table_obj.dbname && table_obj.tablename){    //如果沒有db name & table name就不產出json file
            //寫入json file
            writeJson(table_obj);
        }
        
        table_obj = {
            dbname: '',
            tablename: '',
            comment: '',
            fields: []
        };
        
        continue;
    }
    
    var tmp_field_obj = {
        fname: '',
        pk: '',
        index: '',
        type: '',
        length: '',
        default: '',
        ori_name: '',
        comment: ''
    };
    
    if(!!arr_push){ //整理table的第二列資料, 這一列包含table name及第一個field
        arr_push = false;
        table_obj.dbname = dbname;
        table_obj.tablename = obj[0].data[data_key][0];
        table_obj.comment = obj[0].data[data_key][1];
        
        //這一段是為了enum這type寫的，因Excel單引號的鳥問題所以需補上單引號
        //Start
        var type_length = '';
        if(obj[0].data[data_key][3] == 'enum'){
            type_length = "'" + obj[0].data[data_key][4];
        }else{
            if(obj[0].data[data_key][4]){
                type_length = obj[0].data[data_key][4];
            }else{
                type_length = 'N';
            }
        }
        //End
 
        tmp_field_obj.fname = obj[0].data[data_key][2];
        tmp_field_obj.pk = (!!obj[0].data[data_key][5] && obj[0].data[data_key][5]=='PK') ? 'Y' : 'N';
        tmp_field_obj.index = (!!obj[0].data[data_key][9] && obj[0].data[data_key][9] == 'v') ? 'Y' : 'N';
        tmp_field_obj.type = obj[0].data[data_key][3];
        tmp_field_obj.length = type_length;
        tmp_field_obj.default = (obj[0].data[data_key][11]) ? obj[0].data[data_key][11] : 'no default';
        tmp_field_obj.ori_name = tmp_field_obj.fname;
        tmp_field_obj.comment = obj[0].data[data_key][6];
        
        table_obj.fields.push(tmp_field_obj);
        continue;
    }else{  //整理table中其他field的資料
        
        //這一段是為了enum這type寫的，因Excel單引號的鳥問題所以需補上單引號
        //Start
        var type_length = '';
        if(obj[0].data[data_key][3] == 'enum'){
            type_length = "'" + obj[0].data[data_key][4];
        }else{
            if(obj[0].data[data_key][4]){
                type_length = obj[0].data[data_key][4];
            }else{
                type_length = 'N';
            }
        }
        //End
        
        tmp_field_obj.fname = obj[0].data[data_key][2];
        tmp_field_obj.pk = (!!obj[0].data[data_key][5] && obj[0].data[data_key][5]=='PK') ? 'Y' : 'N';
        tmp_field_obj.index = (!!obj[0].data[data_key][9] && obj[0].data[data_key][9] == 'v') ? 'Y' : 'N';
        tmp_field_obj.type = obj[0].data[data_key][3];
        tmp_field_obj.length = type_length;
        tmp_field_obj.default = (obj[0].data[data_key][11]) ? obj[0].data[data_key][11] : 'no default';
        tmp_field_obj.ori_name = tmp_field_obj.fname;
        tmp_field_obj.comment = obj[0].data[data_key][6];
        
        table_obj.fields.push(tmp_field_obj);
    }
    var dk = parseInt(data_key) + 1;

    if(arr_push==true || dk == data_length){
        //寫入json file
        writeJson(table_obj);
    }
}

function writeJson(table_obj)
{
    var outputFilepath = './dbschema/'+ table_obj.dbname +'/';
    var outputFilename = outputFilepath + table_obj.tablename +'.json';
    
    //檢查資料夾有無存在，沒有就建立
    if (!fs.existsSync(outputFilepath)){
        fs.mkdirSync(outputFilepath);
    }
    
    fs.writeFile(outputFilename, JSON.stringify(table_obj, null, 4), function(err) {
        if(err) {
            console.log(err);
        }else{
            console.log("JSON saved to " + outputFilename);
        }
    }); 
}
